package org.sakho.tp1;

public class Adapter extends Mobile{
	
	private GroundVehicle gv;
	private Mobile mobile;
	
	public Adapter(){
		
	}
	
	public Adapter(Mobile mobile){
		this.mobile = mobile;
		this.gv = new GroundVehicle();
	}

	public GroundVehicle getGv() {
		return gv;
	}

	public void setGv(GroundVehicle gv) {
		this.gv = gv;
	}
	
	
	public Mobile getMobile() {
		return mobile;
	}

	public void setMobile(Mobile mobile) {
		this.mobile = mobile;
	}

	public void deplacer(double DirectionDeg){
		gv.setX(mobile.getX());
		gv.setY(mobile.getY());
		gv.move(DirectionDeg *(180/Math.PI));
		mobile.setX(gv.getX());
		mobile.setY(mobile.getY());
		
	}
	
	

}
