package org.sakho.tp1;

public class AffichageMonde implements Command {
	
	private Monde monde;
	
	public AffichageMonde(){
		super();
	}
	
	public AffichageMonde(Monde monde){
		super();
		this.monde = monde;
		
	}
	
	public void execute(){
		monde.AfficherListVeg();
	}

}
