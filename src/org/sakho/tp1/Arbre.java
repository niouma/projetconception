package org.sakho.tp1;

public class Arbre extends Vegetal{
	
	private double taille;
	private ArbreType type;
	
	public Arbre(){
		
	}
	
	public Arbre(String type, double taille, double x, double y){
		super(x,y);
		this.type = Monde.getInstance().getGat().getArbreType(type);
		this.taille = taille;
		System.out.println("Creation arbre de type " + type + " : position (" + this.getX() + ", " + this.getY() +") , taille " + taille);
		
		
	}
	
	public double getTaille() {
		return taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}
	
	public void dessiner(){
		System.out.println("Dessin arbre de type " + type.getType() + " : position (" + this.getX() + ", " + this.getY() +") , taille " + taille);
	}
	
	public void grandir(double f){
		taille = taille*f;
	}
	

}
