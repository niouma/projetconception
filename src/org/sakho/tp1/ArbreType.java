package org.sakho.tp1;

public class ArbreType {
	
	private double[][] texture;
	private String type;
	
	public ArbreType(String type){
		this.texture = new double[100][100];
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double[][] getTexture() {
		return texture;
	}

	public void setTexture(double[][] texture) {
		this.texture = texture;
	}
	
	

}
