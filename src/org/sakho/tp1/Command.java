package org.sakho.tp1;

public interface Command {
	
	public void execute();

}
