package org.sakho.tp1;

public class DecoreFruit extends VegetalDecore{
	
	public DecoreFruit(){
		super();
	}
	
	public DecoreFruit(Vegetal vegetal){
		super(vegetal);
	}
	
	public void dessiner(){
		super.getVegetal().dessiner();
		System.out.println("+ Ajout du decor FRUITS sur arbre (" + super.getVegetal().getX() + ", " +  super.getVegetal().getY() + ")" );
	}
	
	

}
