package org.sakho.tp1;

public class DecoreNeige extends VegetalDecore {
	
	public DecoreNeige(){
		super();
	}
	
	public DecoreNeige(Vegetal vegetal){
		super(vegetal);
	}
	
	public void dessiner(){
		super.getVegetal().dessiner();
		System.out.println("+ Ajout du decor NEIGE sur arbre (" + super.getVegetal().getX() + ", " +  super.getVegetal().getY() + ")" );
	}

}
