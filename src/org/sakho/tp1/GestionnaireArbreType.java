package org.sakho.tp1;


import java.util.HashMap;
import java.util.Map;

public class GestionnaireArbreType {
	
	private HashMap<String,ArbreType> arbresType;
	
	public GestionnaireArbreType() {
		arbresType = new HashMap<String,ArbreType>();
	}
	
	public ArbreType getArbreType(String type){
		if (arbresType.containsKey(type))
			return arbresType.get(type);
		else{
			ArbreType at = new ArbreType(type);
			arbresType.put(type, at);
			System.out.println("Nouveau type " + type + " ajoute");
			return at;
		}
		
	}

	public HashMap<String, ArbreType> getArbresType() {
		return arbresType;
	}
	
	public void saison(){
		for (Map.Entry<String, ArbreType> entry :arbresType.entrySet()) {
			entry.getValue().setTexture(new double[200][200]);
		}
	}

	
	

}
