package org.sakho.tp1;

public class Grandir implements Command{
	 private double f;
	 private Monde monde;
	 
	 public Grandir(){
		 super();
	 }
	 
	 public Grandir(double f, Monde monde){
		 super();
		 this.f = f;
		 this.monde = monde;
	 }

	public double getF() {
		return f;
	}

	public void setF(double f) {
		this.f = f;
	}
	 
	public void execute(){
		for(int i=0;i<monde.getListVeg().size();i++) {
			Arbre ar = (Arbre) monde.getListVeg().get(i);
			ar.grandir(f);
		}
	}

}
