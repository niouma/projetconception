package org.sakho.tp1;

public class GroundVehicle {
	
	private double x,y;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public void move(double angleRad){
		x += Math.cos(angleRad);
		y += Math.sin(angleRad);
	}

}
