package org.sakho.tp1;

public abstract class Mobile {
	
	private double x;
	private double y;
	private String nom;
	private Mobile parent;
	
	public Mobile(){
		
	}
	
	public Mobile(double x, double y, String nom) {
		this.x = x;
		this.y = y;
		this.nom = nom;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Mobile getParent() {
		return parent;
	}

	public void setParent(Mobile parent) {
		this.parent = parent;
	}

	public void dessiner(){
		
	}
	
	public void deplacer(double directionDeg){
		
	}
	
	

}
