package org.sakho.tp1;

import java.util.ArrayList;
import java.util.Iterator;


public class MobilePorteur extends Mobile{
	
	private ArrayList<Mobile> mobilePorté;
	
	public MobilePorteur(){
		super();
	}
	
	public MobilePorteur(double x, double y, String nom) {
		super(x,y,nom);
		mobilePorté = new ArrayList<Mobile>();
	}

	public ArrayList<Mobile> getMobilePorté() {
		return mobilePorté;
	}
	
	public void setX(double x){
		Iterator<Mobile> it = mobilePorté.iterator();
		while (it.hasNext()){
			Mobile mob = it.next();
			mob.setX(mob.getX() - super.getX() +x);
		}
		super.setX(x);
	}
	
	public void setY(double y){
		Iterator<Mobile> it = mobilePorté.iterator();
		while (it.hasNext()){
			Mobile mob = it.next();
			mob.setY(mob.getY() - super.getY() + y);
		}
		super.setY(y);
	}
	
	public void AjoutMobile(Mobile mobile){
		mobile.setParent(this);
		mobile.setX(this.getX() + mobile.getX());
		mobile.setY(this.getY() + mobile.getY());
		mobilePorté.add(mobile);
	
	}
	
	public void RetirerMobile(Mobile mobile){
		if (mobilePorté.contains(mobile)) {
			mobilePorté.remove(mobile);
			mobile.setParent(null);
			mobile.setX(mobile.getX() - this.getX());
			mobile.setY(mobile.getY() - this.getY());
		}
	}
	
	public void dessiner(){
		if (this.getParent() == null)
			System.out.println("Mobile Porteur : " + this.getNom() + " (" + this.getX() + ", " + this.getY() +")");
		else
			System.out.println("Mobile Porteur : " + this.getNom() + " (" + this.getX() + ", " + this.getY() +") sur " + this.getParent().getNom());
		for (int i=0;i<mobilePorté.size();i++){
			mobilePorté.get(i).dessiner();
		}
	}
	
	
	
	

	
	

}
