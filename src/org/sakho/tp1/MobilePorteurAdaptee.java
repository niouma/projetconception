package org.sakho.tp1;

public class MobilePorteurAdaptee extends MobilePorteur{
	
	private GroundVehicle gv;
	
	public MobilePorteurAdaptee(){
		super();
		gv = new GroundVehicle();
	}

	public MobilePorteurAdaptee(double x, double y, String nom){
		super(x,y,nom);
		gv = new GroundVehicle();
	}
	
	public void d�placer(double DirectionDeg){
		gv.setX(super.getX());
		gv.setY(super.getY());
		gv.move(DirectionDeg *(180/3.14));
		super.setX(gv.getX());
		super.setY(gv.getY());
		
	}

}
