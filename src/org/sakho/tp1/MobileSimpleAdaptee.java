package org.sakho.tp1;

public class MobileSimpleAdaptee extends MobileSimple{
	
	private GroundVehicle gv;
	
	public MobileSimpleAdaptee(){
		super();
		gv = new GroundVehicle();
	}

	public MobileSimpleAdaptee(double x, double y, String nom){
		super(x,y,nom);
		gv = new GroundVehicle();
	}
	
	public void d�placer(double DirectionDeg){
		gv.setX(super.getX());
		gv.setY(super.getY());
		gv.move(DirectionDeg *(180/Math.PI));
		super.setX(gv.getX());
		super.setY(gv.getY());
		
	}

}
