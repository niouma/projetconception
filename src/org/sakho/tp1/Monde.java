package org.sakho.tp1;

import java.util.ArrayList;

public class Monde {
	
	private static Monde _instance;
	private ArrayList<Vegetal> ListVeg;
	private  GestionnaireArbreType gat;
	private ArrayList<Mobile> ListMob;
	private ArrayList<Command> ListCom;
	
	private Monde() {
		ListVeg = new ArrayList<Vegetal>();
		gat = new GestionnaireArbreType();
		ListMob = new ArrayList<Mobile>();
		ListCom = new ArrayList<Command>();
	}
	
	public static Monde getInstance() {
		if (_instance == null)
			_instance = new Monde();
		return _instance;
	}
	
	public ArrayList<Vegetal> getListVeg() {
		return ListVeg;
	}

	public void setListVeg(ArrayList<Vegetal> listVeg) {
		ListVeg = listVeg;
	}
	
	public ArrayList<Mobile> getListMob() {
		return ListMob;
	}

	public void setListMob(ArrayList<Mobile> listMob) {
		ListMob = listMob;
	}

	public void AfficherListVeg(){
		for(int i=0;i<ListVeg.size();i++){
			ListVeg.get(i).dessiner();
		}
	}
	
	public void AjoutListVeg(Vegetal veg){
		ListVeg.add(veg);
	}
	
	public void RetirerListVeg(Vegetal veg){
		if (ListVeg.contains(veg))
			ListVeg.remove(veg);
	}
	
	public void AfficherListMob(){
		for(int i=0;i<ListMob.size();i++){
			ListMob.get(i).dessiner();
		}
	}
	
	public void AjoutListMob(Mobile mob){
		ListMob.add(mob);
	}
	
	public void RetirerListMob(Mobile mob){
		if (ListMob.contains(mob))
			ListMob.remove(mob);
	}
	
	public Mobile RechercherMobile(Mobile mob){
		if (ListMob.contains(mob))
			return ListMob.get(ListMob.indexOf(mob));
		else
			return null;
	}
	
	public  GestionnaireArbreType getGat() {
		return gat;
	}
	
	public void AjoutListCom(Command com){
		ListCom.add(com);
	}
	
	public void RetirerListCom(Command com){
		if (ListCom.contains(com))
			ListCom.remove(com);
	}
	
	public void executeAll(){
		for (int i=0;i<ListCom.size();i++){
			ListCom.get(i).execute();
		}
	}

	public static void testTP1() {
		// TODO Auto-generated method stub
		
		Monde monde = Monde.getInstance();
		Arbre chene = new Arbre("Chene",4,2.5, 5);
		Arbre sapin = new Arbre("Sapin",1.5,10, 20);
		Arbre peuplier1 = new Arbre("Peuplier",3,6, 15);
		Arbre peuplier2 = new Arbre("Peuplier",2.7,36, 85);
		monde.AjoutListVeg(sapin);
		monde.AjoutListVeg(chene);
		monde.AjoutListVeg(peuplier1);
		monde.AjoutListVeg(peuplier2);
		monde.AfficherListVeg();

	}
	
	public static void testTP2() {
		// TODO Auto-generated method stub
		
		Monde monde = Monde.getInstance();
		Mobile homme = new MobileSimple(20,10,"homme");
		Mobile femme = new MobileSimple(40,20,"femme");
		MobilePorteur v�lo = new MobilePorteur(200,100,"v�lo");
		MobilePorteur bateau = new MobilePorteur(2000,1000,"bateau");
		v�lo.AjoutMobile(femme);
		monde.AjoutListMob(homme);
		monde.AjoutListMob(v�lo);
		System.out.println("*************question 1*****************");
		monde.AfficherListMob();
		monde.RechercherMobile(v�lo).setX(300);
		monde.RechercherMobile(v�lo).setY(200);
		System.out.println("*************question 2*****************");
		monde.AfficherListMob();
		monde.RechercherMobile(v�lo).setX(200);
		monde.RechercherMobile(v�lo).setY(100);
		monde.RetirerListMob(v�lo);
		bateau.AjoutMobile(v�lo);
		monde.AjoutListMob(bateau);
		System.out.println("*************question 3*****************");
		monde.AfficherListMob();
		v�lo.RetirerMobile(femme);
		v�lo.AjoutMobile(homme);
		monde.AjoutListMob(femme);
		monde.RetirerListMob(homme);
		System.out.println("*************question 4*****************");
		monde.AfficherListMob();

	}
	
	public static void testTP3() {
		// TODO Auto-generated method stub
		
		Monde monde = Monde.getInstance();
		Arbre chene = new Arbre("Chene",4,2.5, 5);
		Arbre sapin = new Arbre("Sapin",1.5,10, 20);
		Arbre peuplier1 = new Arbre("Peuplier",3,6, 15);
		Arbre peuplier2 = new Arbre("Peuplier",2.7,36, 85);
		AffichageMonde am = new AffichageMonde(monde);
		monde.AjoutListVeg(sapin);
		monde.AjoutListVeg(chene);
		monde.AjoutListVeg(peuplier1);
		monde.AjoutListVeg(peuplier2);
		Saison s = new Saison(monde.getGat());
		Grandir g = new Grandir(2,monde);
		monde.AjoutListCom(s);
		monde.AjoutListCom(g);
		monde.AjoutListCom(am);
		monde.executeAll();

	}
	
	public static void testTP4() {
		// TODO Auto-generated method stub
		
		Monde monde = Monde.getInstance();
		MobileSimpleAdaptee homme = new MobileSimpleAdaptee(20,10,"homme");
		MobileSimpleAdaptee femme = new MobileSimpleAdaptee(40,20,"femme");
		MobilePorteurAdaptee v�lo = new MobilePorteurAdaptee(200,100,"v�lo");
		v�lo.AjoutMobile(femme);
		monde.AjoutListMob(homme);
		monde.AjoutListMob(v�lo);
		monde.AfficherListMob();
		homme.d�placer(0);
		v�lo.d�placer(90);
		monde.AfficherListMob();
	}
	
	public static void testTP5() {
		// TODO Auto-generated method stub
		Monde monde = Monde.getInstance();
		Arbre pomme = new Arbre("Pommier",4,2.5, 5);
		Arbre poire = new Arbre("Poirier",1.5,10, 20);
		Arbre orange = new Arbre("Oranger",3,6, 15);
		monde.AjoutListVeg(pomme);
		monde.AjoutListVeg(poire);
		monde.AjoutListVeg(orange);
		monde.AfficherListVeg();
		DecoreFruit df1 = new DecoreFruit(pomme);
		monde.RetirerListVeg(pomme);
		DecoreFruit df2 = new DecoreFruit(poire);
		monde.RetirerListVeg(poire);
		DecoreNeige dn = new DecoreNeige(orange);
		monde.RetirerListVeg(orange);
		monde.AjoutListVeg(df1);
		monde.AjoutListVeg(df2);
		monde.AjoutListVeg(dn);
		monde.AfficherListVeg();

	}
	
	public static void main(String[] args) {
		//testTP1();
		//testTP2();
		testTP5();

	}
	
	
}
