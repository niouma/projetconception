package org.sakho.tp1;



public class Saison implements Command{
	
	private GestionnaireArbreType gat;
	
	public Saison(){
		super();
	}
	
	public Saison(GestionnaireArbreType gat){
		super();
		this.gat = gat;
	}
	
	public void execute(){
		gat.saison();
	}
}

