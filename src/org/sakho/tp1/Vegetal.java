package org.sakho.tp1;

public abstract class Vegetal {
	
	protected double x;
	protected double y;
	
	public Vegetal() {
		
	}
	
	public Vegetal(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public void dessiner(){
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	

}
