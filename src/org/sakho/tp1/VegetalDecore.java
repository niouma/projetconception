package org.sakho.tp1;

public abstract class VegetalDecore extends Vegetal{
	
	private Vegetal vegetal;
	
	public VegetalDecore(){
		super();
	}
	
	public VegetalDecore(Vegetal vegetal){
		this.vegetal = vegetal;
	}
	
	

	public Vegetal getVegetal() {
		return vegetal;
	}

	public void setVegetal(Vegetal vegetal) {
		this.vegetal = vegetal;
	}

	public void dessiner(){
		
	}

}
